RPS - NGINX - Ansible Playbooks
===============================

.. image:: https://code.europa.eu/digit-c4/rps/nginx-ansible-playbooks/badges/main/pipeline.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/nginx-ansible-playbooks/-/commits/main
   :alt: Pipeline Status

.. image:: https://code.europa.eu/digit-c4/rps/nginx-ansible-playbooks/-/badges/release.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/nginx-ansible-playbooks/-/releases
   :alt: Latest Release


Content
-------

.. toctree::
   :maxdepth: 2

   howto/index
   architecture/index
   playbooks/index

See Also
--------

* `Config Controller <https://digit-c4.pages.code.europa.eu/rps/config-controller/>`_
* `Docker image for RPS <https://digit-c4.pages.code.europa.eu/rps/proxy/>`_
* `Docker image for WAF <https://digit-c4.pages.code.europa.eu/rps/waf/>`_
* `Ansible Collection for RPS <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection>`_
* `AWX job provisionning <https://digit-c4.pages.code.europa.eu/awx-data/>`_
