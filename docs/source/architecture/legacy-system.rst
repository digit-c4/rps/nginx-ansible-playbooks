Legacy System Requirements
==========================

Each instance of the Reverse Proxy NGINX+ must be installed on a virtual machine
with the following requirements:

* **OS:** Debian 11
* **RAM:** 4 GB

The following ports must be open:

* **80:** HTTP
* **443:** HTTPS
* **22:** SSH

The deployment process expects to find in the **Netbox** the following:

* virtual machines with the role ``RPS`` and a tag named after the environment (e.g. ``copbeta``, ``copprod``)
* a service named ``bind``, or ``bind9``
* a service named ``syslog`` or ``syslog_server``
* mappings with a tag named after the environment (e.g. ``copbeta``, ``copprod``)
