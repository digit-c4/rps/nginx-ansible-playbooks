Continuous Integration / Continuous Deployment
==============================================

Design
------

.. image:: /_static/img/cicd.png
   :alt: schema
   :align: center

Usage
-----

To run the Ansible playbooks in debug mode, specify the ``DEBUG`` variable.

To run a **manual** pipeline, specify the ``MANUAL_TARGET`` variable.

To run a **scheduled** pipeline, specify the ``SCHEDULE_TARGET`` variable.

See the above schema for the valid ``*_TARGET`` values.
