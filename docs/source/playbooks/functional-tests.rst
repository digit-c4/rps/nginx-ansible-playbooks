Functional Tests
================

This playbook is responsible for executing the test suite of a bootstrapped
bubble.

Requirements
------------

* `Hurl <https://hurl.dev>`_

Inventory variables
-------------------

.. csv-table::
   :header: "Variable", "Description", "Default Value"

    ``bubble_name``, "The name of the bubble", N/A
