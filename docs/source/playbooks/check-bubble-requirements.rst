Check Bubble Requirements
=========================

This playbook verifies that a bubble's Netbox contain the required
information needed to provision the RPS Service.

Requirements
------------

* Netbox Plugin DNS
* Netbox Plugin Docker

Environment
-----------

The following environment variables are expected to be set:

.. csv-table::
   :header: "Environment Variable", "Inventory Variable", "Description"
   :widths: 20, 20, 60

   ``NETBOX_API``, ``netbox_api``, "URL to the Netbox API"
   ``NETBOX_TOKEN``, ``netbox_token``, "Token to authenticate to the Netbox API"
   ``BUBBLE_NAME``, ``bubble_name``, "Name of the bubble (as seen in the DNS zones)"
   ``BUBBLE_HAS_EXTERNAL``, ``bubble_has_external``, "Wether the Bubble is accessible via EXTERNAL"
   ``BUBBLE_HAS_INTERNAL``, ``bubble_has_internal``, "Wether the Bubble is accessible via INTERNAL"
   ``BUBBLE_DNS_EXTERNAL_SUFFIX``, ``bubble_dns_external_suffix``, "The suffix of the EXTERNAL DNS Zone (example: ``webcloud.ec.europa.eu``)"
   ``BUBBLE_DNS_INTERNAL_SUFFIX``, ``bubble_dns_internal_suffix``, "The suffix of the INTERNAL DNS Zone (example: ``intracloud.ec.europa.eu``)"

Usage
-----

.. code-block:: shell

   export NETBOX_API="***"
   export NETBOX_TOKEN="***"
   export BUBBLE_NAME="***"
   export BUBBLE_HAS_EXTERNAL="yes"
   export BUBBLE_HAS_INTERNAL="yes"
   export BUBBLE_DNS_EXTERNAL_SUFFIX="webcloud.ec.europa.eu"
   export BUBBLE_DNS_INTERNAL_SUFFIX="intracloud.ec.europa.eu"
   ansible-playbook -l localhost playbooks/check_bubble_requirements/site.yml
