Probe
=====

This playbook is responsible for executing the test suite of the entire RPS
stack.

Requirements
------------

* Netbox Plugin RPS

Roles
-----

This playbook uses the following roles:

* `ec.rps_nginx.probe_healthchecks <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection/roles/probe_healthchecks.html>`_
* `ec.rps_nginx.probe_testurls <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection/roles/probe_testurls.html>`_

Environment
-----------

The following environment variables are expected to be set:

.. csv-table::
   :header: "Environment Variable", "Inventory Variable", "Description"
   :widths: 20, 20, 60

   ``NETBOX_API``, ``netbox_api``, "URL to the Netbox API"
   ``NETBOX_TOKEN``, ``netbox_token``, "Token to authenticate to the Netbox API"
