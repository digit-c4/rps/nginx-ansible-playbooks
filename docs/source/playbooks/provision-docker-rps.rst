Provision Docker RPS
====================

This playbook is responsible for provisionning the entire RPS stack in Netbox.

Requirements
------------

* Netbox Plugin Docker

Roles
-----

This playbook uses the following roles:

* `ec.rps_nginx.provision_docker_router <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection/roles/provision_docker_router.html>`_
* `ec.rps_nginx.provision_docker_proxy <https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-collection/roles/provision_docker_proxy.html>`_

Environment
-----------

The following environment variables are expected to be set:

.. csv-table::
   :header: "Environment Variable", "Inventory Variable", "Description"
   :widths: 20, 20, 60

   ``RPS_DOCKER_REGISTRY_USERNAME``, ``rps_docker_registry_username``, "Username with read access to the RPS docker registry"
   ``RPS_DOCKER_REGISTRY_PASSWORD``, ``rps_docker_registry_password``, "Password for the RPS docker registry"
   ``LB_DOCKER_REGISTRY_USERNAME``, ``router_docker_registry_username``, "Username with read access to the Load Balancer docker registry"
   ``LB_DOCKER_REGISTRY_PASSWORD``, ``router_docker_registry_password``, "Password for the Load Balancer docker registry"
   ``NETBOX_API``, ``netbox_api``, "URL to the Netbox API"
   ``NETBOX_TOKEN``, ``netbox_token``, "Token to authenticate to the Netbox API"
