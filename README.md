# RPS Ansible Playbooks

This repository contains the playbooks used by the RPS squad.

For more information:

 - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute
   to the repository
 - consult the [documentation](https://digit-c4.pages.code.europa.eu/rps/nginx-ansible-playbooks)
