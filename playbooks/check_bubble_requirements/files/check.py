import requests

import json
import sys
import os


NETBOX_API = os.getenv("NETBOX_API")
NETBOX_TOKEN = os.getenv("NETBOX_TOKEN")

BUBBLE_NAME = os.getenv("BUBBLE_NAME")
BUBBLE_HAS_EXTERNAL = os.getenv("BUBBLE_HAS_EXTERNAL", "") in ["t", "true", "y", "yes", "1"]
BUBBLE_HAS_INTERNAL = os.getenv("BUBBLE_HAS_INTERNAL", "") in ["t", "true", "y", "yes", "1"]

BUBBLE_DNS_EXTERNAL_SUFFIX = os.getenv("BUBBLE_DNS_EXTERNAL_SUFFIX")
BUBBLE_DNS_INTERNAL_SUFFIX = os.getenv("BUBBLE_DNS_INTERNAL_SUFFIX")

REQUIREMENTS = {
    "cmdb": {
        "netbox_plugins": [
            "netbox_dns",
            "netbox_rps_plugin",
            "netbox_cert_plugin",
            "netbox_docker_plugin",
        ],
    },
    "sys": {
        "vip_external": BUBBLE_HAS_EXTERNAL,
        "vip_internal": BUBBLE_HAS_INTERNAL,
        "rps_virtual_machines": [
            "rps.ec.local",
            "rps-2.ec.local",
            "rps-3.ec.local",
            "rps-4.ec.local",
        ],
        "docker_hosts": [
            "rps.ec.local",
            "rps-2.ec.local",
            "rps-3.ec.local",
            "rps-4.ec.local",
        ],
        "services": {
            ("netbox", "netbox.ec.local", 8080): True,
            ("awx", "awx.ec.local", 8013): True,
            ("proxy", "proxy.ec.local", 8012): True,
            ("openobserve", "openobserve.ec.local", 5080): True,
            ("prometheus", "prometheus.ec.local", 9090): True,
            ("alertmanager", "alertmanager.ec.local", 9093): True,
            ("syslog_server", "syslog.ec.local", 5514): True,
            ("DNS_TCP", "dns-private.ec.local", 53): True,
            ("DNS_UDP", "dns-private.ec.local", 53): True,
            ("DNS_TCP", "dns-public.ec.local", 53): True,
            ("DNS_UDP", "dns-public.ec.local", 53): True,
            ("nginx", "rps.ec.local", 443): True,
            ("nginx", "rps-2.ec.local", 443): True,
            ("nginx", "rps-3.ec.local", 443): True,
            ("nginx", "rps-4.ec.local", 443): True,
        },
    },
    "dns": {
        "zones": {
            ("ec.local", "INTERNAL"): True,
            (f"{BUBBLE_NAME}.nms.tech.ec.europa.eu", "INTERNAL"): True,
            (f"{BUBBLE_NAME}.{BUBBLE_DNS_EXTERNAL_SUFFIX}", "EXTERNAL"): BUBBLE_HAS_EXTERNAL,
            (f"{BUBBLE_NAME}.{BUBBLE_DNS_INTERNAL_SUFFIX}", "INTERNAL"): BUBBLE_HAS_INTERNAL,
        },
        "records": {
            ("netbox", "ec.local"): True,
            ("awx", "ec.local"): True,
            ("proxy", "ec.local"): True,
            #("openobserve", "ec.local"): True,
            #("prometheus", "ec.local"): True,
            #("alertmanager", "ec.local"): True,
        },
    },
}


def check_netbox_plugins(observed, desired):
    """ Check that NetBox plugins are installed """

    missing = [
        plugin
        for plugin in desired["cmdb"]["netbox_plugins"]
        if plugin not in observed["cmdb"]["netbox_plugins"]
    ]

    if len(missing) > 0:
        return False, f"Missing NetBox plugins: {', '.join(missing)}"

    return True, "NetBox plugins are installed"


def check_vip_external(observed, desired):
    """ Check if external VIP is present """

    has_vip = observed["sys"]["vip_external"]

    if desired["sys"]["vip_external"] and not has_vip:
        return False, "Missing external VIP"

    if not desired["sys"]["vip_external"] and has_vip:
        return True, "Unexpected external VIP"

    return True, "External VIP is present"


def check_vip_internal(observed, desired):
    """ Check if internal VIP is present """

    has_vip = observed["sys"]["vip_internal"]

    if desired["sys"]["vip_internal"] and not has_vip:
        return False, "Missing internal VIP"

    if not desired["sys"]["vip_internal"] and has_vip:
        return True, "Unexpected internal VIP"

    return True, "Internal VIP is present"


def check_rps_virtual_machines(observed, desired):
    """ Check that RPS virtual machines are present """

    missing = [
        vm
        for vm in desired["sys"]["rps_virtual_machines"]
        if vm not in observed["sys"]["rps_virtual_machines"]
    ]

    if len(missing) > 0:
        return False, f"Missing RPS virtual machines: {', '.join(missing)}"

    return True, "RPS virtual machines are present"


def check_docker_hosts(observed, desired):
    """ Check that Docker hosts are present """

    missing = [
        host
        for host in desired["sys"]["docker_hosts"]
        if host not in observed["sys"]["docker_hosts"]
    ]

    if len(missing) > 0:
        return False, f"Missing Docker hosts: {', '.join(missing)}"

    return True, "Docker hosts are present"


def check_services(observed, desired):
    """ Check that services are present """

    missing = [
        f"{name} ({vm}:{port})"
        for (name, vm, port) in desired["sys"]["services"].keys()
        if (
            desired["sys"]["services"][(name, vm, port)]
            and not observed["sys"]["services"].get((name, vm, port))
        )
    ]

    if len(missing) > 0:
        return False, f"Missing services: {', '.join(missing)}"

    return True, "Services are present"


def check_dns_zones(observed, desired):
    """ Check that DNS zones are present """

    missing = [
        f"[{view}] {zone}"
        for zone, view in desired["dns"]["zones"].keys()
        if (
            desired["dns"]["zones"][(zone, view)]
            and not observed["dns"]["zones"].get((zone, view))
        )
    ]

    if len(missing) > 0:
        return False, f"Missing DNS zones: {', '.join(missing)}"

    return True, "DNS zones are present"


def check_dns_records(observed, desired):
    """ Check that DNS records are present """

    missing = [
        f"{record}.{zone}"
        for record, zone in desired["dns"]["records"].keys()
        if (
            desired["dns"]["records"][(record, zone)]
            and not observed["dns"]["records"].get((record, zone))
        )
    ]

    if len(missing) > 0:
        return False, f"Missing DNS records: {', '.join(missing)}"

    return True, "DNS records are present"


def main():
    client = Client()
    observer = Observer(client)

    desired_state = REQUIREMENTS
    observed_state = observer.fetch()

    checks = [
        check_netbox_plugins,
        check_vip_external,
        check_vip_internal,
        check_rps_virtual_machines,
        check_docker_hosts,
        check_services,
        check_dns_zones,
        check_dns_records,
    ]

    success = True
    report = []

    for check in checks:
        result, message = check(observed_state, desired_state)
        success = success and result
        report.append({
            "check": check.__doc__.strip(),
            "message": message,
        })

    print(json.dumps(report))
    sys.exit(0 if success else 1)


class Client(requests.Session):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.headers.update({"Authorization": f"Token {NETBOX_TOKEN}"})

    def request(self, method, path, *args, **kwargs):
        return super().request(method, f"{NETBOX_API}{path}", *args, **kwargs)


class Observer:
    def __init__(self, client):
        self.client = client
        self.data = {
            "cmdb": {},
            "sys": {},
            "dns": {},
        }

    def fetch(self):
        self._fetch_plugins()
        self._fetch_vip_external()
        self._fetch_vip_internal()
        self._fetch_rps_virtual_machines()
        self._fetch_docker_hosts()
        self._fetch_services()
        self._fetch_dns_zones()
        self._fetch_dns_records()

        return self.data

    def _fetch_plugins(self):
        resp = self.client.get("/api/plugins/installed-plugins")
        resp.raise_for_status()
        data = resp.json()

        self.data["cmdb"]["netbox_plugins"] = [
            plugin["package"]
            for plugin in data
        ]

    def _fetch_vip_external(self):
        if BUBBLE_HAS_EXTERNAL:
            resp = self.client.get("/api/ipam/ip-addresses/", params={
                "role": "vip",
                "tag": "vip_external",
            })
            resp.raise_for_status()
            data = resp.json()

            self.data["sys"]["vip_external"] = len(data["results"]) > 0

        else:
            self.data["sys"]["vip_external"] = None

    def _fetch_vip_internal(self):
        if BUBBLE_HAS_INTERNAL:
            resp = self.client.get("/api/ipam/ip-addresses/", params={
                "role": "vip",
                "tag": "vip_internal",
            })
            resp.raise_for_status()
            data = resp.json()

            self.data["sys"]["vip_internal"] = len(data["results"]) > 0

        else:
            self.data["sys"]["vip_internal"] = None

    def _fetch_rps_virtual_machines(self):
        resp = self.client.get("/api/virtualization/virtual-machines/", params={
            "role": "rps",
        })
        resp.raise_for_status()
        data = resp.json()

        self.data["sys"]["rps_virtual_machines"] = [
            vm["name"]
            for vm in data["results"]
        ]

    def _fetch_docker_hosts(self):
        resp = self.client.get("/api/plugins/docker/hosts/?state=running")
        resp.raise_for_status()
        data = resp.json()

        self.data["sys"]["docker_hosts"] = [
            host["name"]
            for host in data["results"]
        ]

    def _fetch_services(self):
        resp = self.client.get("/api/ipam/services/")
        resp.raise_for_status()
        data = resp.json()

        self.data["sys"]["services"] = {
            (svc["name"], svc["virtual_machine"]["name"], svc["ports"][0]): True
            for svc in data["results"]
        }

    def _fetch_dns_zones(self):
        resp = self.client.get("/api/plugins/netbox-dns/zones/")
        resp.raise_for_status()
        data = resp.json()

        self.data["dns"]["zones"] = {
            (zone["name"], zone["view"]["name"]): True
            for zone in data["results"]
        }

    def _fetch_dns_records(self):
        resp = self.client.get("/api/plugins/netbox-dns/records/")
        resp.raise_for_status()
        data = resp.json()

        self.data["dns"]["records"] = {
            (record["name"], record["zone"]["name"]): True
            for record in data["results"]
        }


if __name__ == "__main__":
    try:
        main()

    except Exception as err:
        print(json.dumps({"error": str(err)}))
        sys.exit(1)
