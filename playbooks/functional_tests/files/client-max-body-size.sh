#!/usr/bin/env bash

set -e

< /dev/urandom tr -dc 'a-zA-Z0-9' | head -c 1M > file_1M.txt
< /dev/urandom tr -dc 'a-zA-Z0-9' | head -c 128M > file_128M.txt
< /dev/urandom tr -dc 'a-zA-Z0-9' | head -c 130M > file_130M.txt
< /dev/urandom tr -dc 'a-zA-Z0-9' | head -c 2G > file_2G.txt

function cleanup() {
  rm -f file_1M.txt file_128M.txt file_130M.txt file_2G.txt
}

trap cleanup EXIT
trap cleanup SIGINT

cat <<EOF | hurl $@
POST https://test-c4hou-128M.{{ bubble_name }}.nms.tech.ec.europa.eu/upload
[Options]
file,file_1M.txt;
HTTP 200

POST https://test-c4hou-128M.{{ bubble_name }}.nms.tech.ec.europa.eu/upload
[Options]
file,file_128M.txt;
HTTP 200

POST https://test-c4hou-128M.{{ bubble_name }}.nms.tech.ec.europa.eu/upload
[Options]
file,file_130M.txt;
HTTP 413

POST https://test-c4hou-2G.{{ bubble_name }}.nms.tech.ec.europa.eu/upload
[Options]
file,file_2G.txt;
HTTP 200

EOF
